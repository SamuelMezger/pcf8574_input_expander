from pcf8574_input_expander import PCF8574, InterruptPin, SMBusReader
from RPi import GPIO  # only needed for cleanup
import time
# %%

input_module = PCF8574(SMBusReader(0x20), InterruptPin(4))


def print_what_changed(pin_index, is_tapped_down):
    print(f"pin #{pin_index} {'down' if is_tapped_down else 'up'}")


input_module.set_callback(print_what_changed)
input_module.set_per_pin_callback(
    3,
    lambda is_tapped_down: print("I'm a special pin, therefore I get my own callback\n" * is_tapped_down, end=''),
)

# %%
try:
    print("waiting ...")
    while True:
        # sleeping in main thread, interrupts will run as demons
        time.sleep(60)
except KeyboardInterrupt:
    GPIO.cleanup()
