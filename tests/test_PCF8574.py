import time
from unittest import TestCase
from unittest.mock import Mock
from pcf8574_input_expander.PCF8574 import PCF8574


class TestPCF8574(TestCase):
    def test_who_triggered(self):

        def send_int_with(ans):
            sm_bus_reader.read.return_value = ans
            pcf8574.on_interrupt()

        answers = [
            0b11110111,  # 3, True
            0b11010111,  # 5, True
            0b11011101,  # 3, False  1, True
            0b11011111,  # 1, False
            0b11111111,  # 5, False
            # 0b11011101,
        ]
        expected = [
            {(3, True)},
            {(5, True)},
            {(3, False), (1, True)},
            {(1, False)},
            {(5, False)}
        ]

        def callback(pin_index, is_tapped_down):
            print(f"{'':<30}{pin_index}, {is_tapped_down}")
            changed.add((pin_index, is_tapped_down))

        sm_bus_reader = Mock()
        interrupt_pin = Mock()
        sm_bus_reader.read.return_value = 0b11111111  # for initial_state in __init__()
        pcf8574 = PCF8574(sm_bus_reader, interrupt_pin)
        pcf8574.callback = callback

        changed = set()
        for ans, expected in zip(answers, expected):

            send_int_with(ans)
            time.sleep(.1)
            # self.assertEqual(expected, changed)
            changed.clear()

        print(list(zip(answers, expected)))
        time.sleep(1)

    def test_bits(self):

        def decompose(x):
            powers = []
            i = 1
            while i <= x:
                if i & x:
                    powers.append(i)
                i <<= 1
            return powers

        def get_bit(y, x):
            return str((x >> y) & 1)

        def tobin(x, count=8):
            shift = range(count - 1, -1, -1)
            bits = map(lambda y: get_bit(y, x), shift)
            return "".join(bits)

        print(tobin(-0b00100000))
