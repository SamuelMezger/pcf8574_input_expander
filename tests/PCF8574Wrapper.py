from pcf8574_input_expander import PCF8574, InterruptPin, SMBusReader


class PCF8574Wrapper:
    def __init__(self, addr, interrupt_pin):
        self.pcf8574 = PCF8574(SMBusReader(addr), InterruptPin(interrupt_pin))
        self.pcf8574.set_callback(self.on_entity_changed)

    def on_entity_changed(self, pin_index, is_tapped_down):
        print(bin(self.pcf8574.debounced_state))
        print(f"{'':<30}{pin_index}, {is_tapped_down}")
