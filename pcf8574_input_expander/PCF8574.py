import threading


class PCF8574:
    def __init__(self, sm_bus_reader, interrupt_pin, bounce_time: int = 100):
        self.threading_lock = threading.Lock()
        self.sm_bus_reader = sm_bus_reader
        self.last_read = 0b11111111  # used to track real state
        self.debounced_state = 0b11111111  # used to track debounced state

        self.bounce_time = bounce_time
        self.bounce_lock = 0b00000000

        self.per_pin_callbacks = [lambda _: None] * 8
        self.callback = lambda pin_index, is_tapped_down: None
        self.on_interrupt()
        interrupt_pin.add_callback(self.on_interrupt)

    def on_interrupt(self, _=None):
        with self.threading_lock:
            self.who_triggered()

    def who_triggered(self):
        new_read = self.sm_bus_reader.read()
        difference = self.last_read - new_read
        changed_mask = new_read ^ self.last_read
        self.last_read = new_read

        handled_diff = 0b0000000
        # each pin that changed is handled in one iteration
        # while there is a difference between the current and last bus read that isn't yet handled
        while difference:
            changed_mask = changed_mask ^ abs(handled_diff)
            pin_index = changed_mask.bit_length() - 1

            is_tapped_down = difference > 0
            plus_or_minus_1 = (is_tapped_down * 2) - 1
            handled_diff = plus_or_minus_1 << pin_index

            # if the input isn't locked
            if not (self.bounce_lock & abs(handled_diff)):
                # update state
                self.change_state(handled_diff, pin_index, is_tapped_down)
                # add input to lock
                self.bounce_lock += abs(handled_diff)
                # start timer to unlock the pin as soon as bounce_time has elapsed
                threading.Timer(self.bounce_time / 1000, self.un_lock, [handled_diff, pin_index, is_tapped_down]).start()
            difference = difference - handled_diff

    def un_lock(self, handled_diff, pin_index, was_tapped_down):
        with self.threading_lock:
            self.bounce_lock -= abs(handled_diff)
            # if the pin changed while it was locked we need to artificially change the debounced state to the current
            if (was_tapped_down and self.nth_bit_is_set(self.last_read, pin_index)) or (not was_tapped_down and not self.nth_bit_is_set(self.last_read, pin_index)):
                self.change_state(-handled_diff, pin_index, not was_tapped_down)

    def change_state(self, handled_diff, pin_index, is_tapped_down):
        self.debounced_state -= handled_diff
        self.callback(pin_index, is_tapped_down)
        self.per_pin_callbacks[pin_index](is_tapped_down)

    def set_callback(self, callback):
        self.callback = callback

    def set_per_pin_callback(self, pin_index, callback):
        self.per_pin_callbacks[pin_index] = callback

    def get_state_of_pin(self, pin_index):
        is_tapped_down = not self.nth_bit_is_set(self.debounced_state, pin_index)
        return is_tapped_down

    @staticmethod
    def nth_bit_is_set(x, n):
        return x & (1 << n)
