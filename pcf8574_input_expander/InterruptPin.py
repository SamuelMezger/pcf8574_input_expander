from RPi import GPIO


class InterruptPin:
    def __init__(self, pin_number, gpio_mode=GPIO.BCM):
        GPIO.setmode(gpio_mode)
        self.pin_number = pin_number
        GPIO.setup(pin_number, GPIO.IN, pull_up_down=GPIO.PUD_UP)

    def add_callback(self, callback):
        GPIO.add_event_detect(self.pin_number, GPIO.FALLING, callback=callback)

