from smbus2 import SMBus


class SMBusReader:
    def __init__(self, addr):
        self.addr = addr

    def read(self):
        with SMBus(1) as bus:
            return bus.read_byte(self.addr)
