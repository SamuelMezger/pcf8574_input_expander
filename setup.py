import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="pcf8574_input_expander",  # Replace with your own username
    # version="0.0.1",
    # author="Samuel Mezger",
    # author_email="samuel.mezger@gmx.de",
    # description="For reading pcf8574 input expander and adding callbacks to pin events",
    # long_description=long_description,
    # long_description_content_type="text/markdown",
    # url="https://github.com/pypa/sampleproject",
    # packages=setuptools.find_packages(),
    install_requires=[
        'smbus2',
        'RPi.GPIO'
    ],
    # classifiers=[
    #     "Programming Language :: Python :: 3",
    #     "License :: OSI Approved :: MIT License",
    #     "Operating System :: OS Independent",
    # ],
    # python_requires='>=3.6',
)
