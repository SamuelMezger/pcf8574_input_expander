### **Python package for PCF8574 I<s>/O</s> Expansion board**
For reading current state and adding callbacks to pin events (interrupt handling).

Also supports per-pin debouncing to prevent multiple state-changes when pressing a button/switch.

This module can not use the PCF8574 pins in output/write mode
(see [pcf8574-io package](https://pypi.org/project/pcf8574-io) for this functionality, but it can't handle interrupts).

Usage Example:

```python
from pcf8574_input_expander import PCF8574, InterruptPin, SMBusReader
from RPi import GPIO  # only needed for cleanup
import time


input_module = PCF8574(SMBusReader(0x20), InterruptPin(4))


def print_what_changed(pin_index, is_tapped_down):
    print(f"pin #{pin_index} {'down' if is_tapped_down else 'up'}")


input_module.set_callback(print_what_changed)
input_module.set_per_pin_callback(
    3, lambda _: print("I'm a special pin, therefore I get my own callback")
)

try:
    print("waiting for something to happen...")
    while True:
        time.sleep(60)
except KeyboardInterrupt:
    GPIO.cleanup()
```

Example output:
```
waiting for something to happen...
pin #0 down
pin #0 up
pin #1 down
pin #1 up
pin #3 down
I'm a special pin, therefore I get my own callback
pin #3 up
I'm a special pin, therefore I get my own callback
pin #6 down
pin #6 up
pin #7 down
pin #7 up
```
Developed for the Raspberry Pi, requires the python-smbus2 package to access the I2C bus and RPi.GPIO for detecting the interrupt.
